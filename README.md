# A demonstration of testing against redis using GitLab CI.

A sample application to demonstrate continuous integration in Gitlab.

Use docker services:
```
services:
  - redis:latest
```

In testing code, we need to use `"redis"` (not `"127.0.0.1"`) when we try to connect to the redis service.
```scala
val r = new RedisClient("redis", 6379)
```

## Code
The project content is based on the Activator `minimal-scala` template.

## Prerequisites
You will need to have **Activator** installed to run the project.

## Run tests
Execute `activator test` in the project root directory. This project will run tests against `redis`

