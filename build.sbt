name := """sample-redis-service"""

version := "1.0"

scalaVersion := "2.11.11"

libraryDependencies ++= Seq(
  "net.debasishg" %% "redisclient" % "3.4",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test"
)
