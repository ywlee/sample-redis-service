package com.example

import com.redis._

class RedisAccessor(address: String, port: Int) {
  val r = new RedisClient(address, port)
  
  def setValue(key: String, value: String) = {
    r.set(key, value)
  }

  def getValue(key: String) = {
    r.get(key)
  }
}

object Hello {
  def main(args: Array[String]): Unit = {
    val redis = new RedisAccessor("127.0.0.1", 6379)
    redis.setValue("abc", "123")
    println(redis.getValue("abc"))
  }
}
