package com.example

import org.scalatest._

class RedisAccessorSpec extends FlatSpec with Matchers {
  "RedisAccessor" should "get value successfully" in {
    val r = new RedisAccessor("redis", 6379)
    r.setValue("abc", "456")
    
    r.getValue("abc") should === (Some("456"))
  }
}
